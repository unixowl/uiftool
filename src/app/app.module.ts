import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SortableModule } from 'ngx-bootstrap/sortable';
import { AppComponent } from './app.component';
import { SimpleItemsDemoComponent} from './simple-items.component';



@NgModule({
  declarations: [
    AppComponent,
    SimpleItemsDemoComponent
  ],
  imports: [
    BrowserModule,
    SortableModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
