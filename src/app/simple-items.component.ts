import { Component } from '@angular/core';

@Component({
  selector: 'app-simple-items-demo',
  templateUrl: './simple-items.components.html'
})

export class SimpleItemsDemoComponent {
  public itemStringsLeft: any[] = [
    'Windstorm',
    'Bombasto',
    'Magneta',
    'Tornado'
  ];

  public itemStringsRight: any[] = [
    'Mr. O',
    'Tomato'
  ];
}
